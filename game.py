import dijkstar
import itertools
import string

class Square:
    def read(raw):
        if isinstance(raw, str):
            rank, file = raw
            return Square(rank, int(file))

        return raw

    def __init__(self, rank, file):
        self.rank = rank
        self.file = file

    def __eq__(self, other):
        return self.rank == other.rank and self.file == other.file

    def __str__(self):
        return f"{self.rank}{self.file}"

    def __repr__(self):
        return self.__str__()

    def __hash__(self):
        return hash(str(self))

class Queen(Square):
    pass

class Knight(Square):
    def see_queen(self, queen):
        rise = self.file - queen.file
        run  = ord(self.rank) - ord(queen.rank)

        try:
            slope = rise / run

            return any([rise == 0, run == 0, abs(slope) == 1])
        except ZeroDivisionError:
            return True

    def knight_moves(self):
        offsets = range(-2, 3, 1)
        rn = ord(self.rank)
        ranks = map(lambda offset: chr(rn + offset), offsets)
        files = map(lambda offset: self.file + offset, offsets)

        # https://www.chessvariants.com/large.dir/elves.html
        seeress = [Knight(rank, file) for rank, file in itertools.product(ranks, files)]

        return filter(lambda s: not s.see_queen(self), seeress)

class Game:
    def __init__(self, queen):
        queen = Queen.read(queen)
        ranks = string.ascii_lowercase[7::-1]
        files = range(8, 0, -1)
        squares = (Knight(rank, file) for file, rank in itertools.product(files, ranks))

        self.knights = list(filter(lambda square: not square.see_queen(queen), squares))

        self.graph = dijkstar.Graph(undirected=True)

        for k1 in self.knights:
            for k2 in k1.knight_moves():
                if k2 in self.knights:
                    self.graph.add_edge(k1, k2, 1)

    def knight_from_to(self, k1, k2):
        k1 = Knight.read(k1)
        k2 = Knight.read(k2)

        if any(knight not in self.knights for knight in [k1, k2]):
            return []

        return dijkstar.find_path(self.graph, k1, k2).nodes
