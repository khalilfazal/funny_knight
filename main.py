#!/bin/env python3

from game import Game

import functools
import pyautogui

from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options

import time

def select_square(square):
    return (By.CSS_SELECTOR, f'div[data-squareid="{square}"]')

@functools.lru_cache(maxsize=36)
def find_knight(browser, knight):
    return browser.find_element(*select_square(str(knight)))

@functools.lru_cache(maxsize=36)
def center(element):
    rect = element.rect
    return (rect['x'] + rect['width'] / 2, rect['y'] + rect['height'] / 2)

def knight_move(k1, k2):
    k1c = center(k1)
    k2c = center(k2)

    pyautogui.mouseDown(k1c)
    pyautogui.dragTo(k2c, duration=0.2)

def stagger(list):
    return zip(list[:-1], list[1:])

def main():
    pyautogui.FAILSAFE = False
    pyautogui.PAUSE = 0

    browser_options = Options()
    browser_options.add_argument('--kiosk');
    browser = webdriver.Firefox(options=browser_options)

    # browser = webdriver.Remote(
    #     command_executor="http://127.0.0.1:4444/wd/hub",
    #     desired_capabilities = webdriver.DesiredCapabilities.HTMLUNITWITHJS
    # )

    uri = 'https://www.funnyhowtheknightmoves.com/'
    browser.get(uri)

    g = Game('d5')
    order = g.knights

    moves = 0
    for current, destination in stagger(order):
        path = g.knight_from_to(current, destination)

        for k1, k2 in stagger(path):
            k1 = find_knight(browser, k1)
            k2 = find_knight(browser, k2)

            # ActionChains(browser)\
            #     .move_to_element(k1)\
            #     .pause(1)\
            #     .click_and_hold(k1)\
            #     .pause(1)\
            #     .move_by_offset(1, 0)\
            #     .move_to_element(k2)\
            #     .move_by_offset(1, 0)\
            #     .pause(1)\
            #     .release()\
            #     .perform()

            # new_knight = browser.find_element(By.CSS_SELECTOR, 'div[data-testid^="wN"]')
            # print(new_knight.get_attribute('data-testid'))

            knight_move(k1, k2)
            moves += 1

            if moves % 2 == 0:
                time.sleep(0.12)

    browser.quit()

if __name__ == '__main__':
    main()